import React from 'react';
import ReactDOM from 'react-dom';
import AttractLoop from './AttractLoop';

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

const initialState = {}; 
const mockStore = configureStore();

it('renders without crashing', () => {
  const store = mockStore(initialState)
  const div = document.createElement('div');
  
  ReactDOM.render(
    <Provider store={store}>
      <AttractLoop />
    </Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
