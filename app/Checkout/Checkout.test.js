import React from 'react';
import ReactDOM from 'react-dom';
import Checkout from './Checkout';

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

const initialState = {}; 
const mockStore = configureStore();

it('renders without crashing', () => {
  const store = mockStore(initialState)
  const div = document.createElement('div');
  
  ReactDOM.render(
    <Provider store={store}>
      <Checkout />
    </Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
