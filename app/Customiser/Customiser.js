import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as BABYLON from 'babylonjs';
import Scene from '../Scene/Scene';

import { history } from '../store/configureStore';

import update from 'immutability-helper';

import './Customiser.css';

import ColorTool from './Tools/ColorTool';
import PatternTool from './Tools/PatternTool';
import TextTool from './Tools/TextTool';

const parts = [
  {
    id: 0,
    name: 'Pen Body',
    mesh: '01_body_Cube.003',
    type: 'color',
    value: ''
  },
  {
    id: 1,
    name: 'Pen Cap',
    mesh: '01_cap_Cube.002',
    type: 'color',
    value: ''
  },
  {
    id: 2,
    name: 'Pen Point',
    mesh: '01_point_Cube',
    type: 'color',
    value: ''
  },
  {
    id: 3,
    name: 'Pen Body Pattern',
    mesh: '01_body_imgtxt_Cube.004',
    type: 'pattern',
    value: ''
  },
  {
    id: 4,
    name: 'Pen Cap Text',
    mesh: '01_cap_imgtxt_Cube.005',
    type: 'text',
    value: ''
  }
]

class Customiser extends Component {

  canvas = {};
  scene = {};
  engine = {};

  state = {
    modelsLoaded: false,
    parts
  }

  onSceneMount = (e) => {

    console.log(this.props.public);

    this.canvas = e.canvas;
    this.scene = e.scene;
    this.engine = e.engine;

    this.scene.debugLayer.show();

    this.scene.clearColor = new BABYLON.Color3.FromHexString("#EEEEEE");

    this.hemiLight = new BABYLON.HemisphericLight("HemiLight", new BABYLON.Vector3(0, 0, 0), this.scene);
    // this.hemiLight.intensity = 0.6;
    this.light = new BABYLON.PointLight("Omni", new BABYLON.Vector3.Zero(), this.scene);
    this.light.intensity = .6;

    this.camera = new BABYLON.ArcRotateCamera("Camera", 0, 0, 0, BABYLON.Vector3.Zero(), this.scene);
    this.camera.upperRadiusLimit = 50;
    this.camera.lowerRadiusLimit = 10;
    this.camera.attachControl(this.canvas, true);
    // this.scene.activeCamera.alpha += Math.PI;
    this.scene.activeCamera.radius = 15;

    BABYLON.OBJFileLoader.OPTIMIZE_WITH_UV = true;
    BABYLON.SceneLoader.ShowLoadingScreen = false;
    // BABYLON.SceneLoader.Append("http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/", "Pen.obj", this.scene, (scene) => {
    BABYLON.SceneLoader.Append(this.props.public, "Pen.obj", this.scene, (scene) => {
      
      this.setState({modelsLoaded: true});

      scene.meshes.forEach((mesh) => {
        mesh.rotation = new BABYLON.Vector3( - Math.PI, Math.PI / 4,Math.PI / 2);
      })
      
    });

    this.scene.registerBeforeRender(() => {
      this.light.position = this.camera.position;
    });

    this.engine.runRenderLoop(() => {
        if (this.scene) {
          this.scene.render();
          this.engine.resize();
        }
    });

  }

  resetCamera = () => {
    // var alphaAnim 	= new BABYLON.Animation ("alphaAnim", "alpha", 300, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    // var betaAnim 	= new BABYLON.Animation ("betaAnim", "beta", 300, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    // var alphaKeys 	= [{frame: 0, value: this.scene.activeCamera.alpha}, {frame: 100, value: 0}];
    // var betaKeys 	= [{frame: 0, value: this.scene.activeCamera.beta},  {frame: 100, value: 0}];
    
    // alphaAnim.setKeys(alphaKeys);
    // betaAnim.setKeys(betaKeys);	
    
    // this.scene.activeCamera.animations.push(alphaAnim);	
    // this.scene.activeCamera.animations.push(betaKeys);
    // this.scene.beginAnimation(this.scene.activeCamera, 0, 100, false);
  }

  changeColor = (meshID, colorHex) => {

    const materialName = `material-${meshID}`;

    if(this[materialName]){
      this[materialName].dispose();
    }

    var color = new BABYLON.Color3.FromHexString(colorHex);
    this[materialName] = new BABYLON.StandardMaterial(materialName, this.scene);
    this[materialName].diffuseColor = color;

    this.scene.getMeshByID(meshID).material = this[materialName];
    
  }

  // changePattern = (meshID, uOffset, vOffset, patternURL) => {
  changePattern = (meshID, patternURL) => {

    const materialName = `material-${meshID}`;
    const textureName = `texture-${meshID}`;

    if(this[materialName]){
      this[materialName].dispose();
    }
    if(this[textureName]){
      this[textureName].dispose();
    }

    // this[textureName]= new BABYLON.Texture("http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/impact.png", this.scene);
    this[textureName]= new BABYLON.Texture(patternURL, this.scene);
    
    this[materialName] = new BABYLON.StandardMaterial("decalMat", this.scene);
		this[materialName].diffuseTexture = this[textureName];
    this[materialName].diffuseTexture.hasAlpha = true;
    // this[materialName].diffuseTexture.uOffset = uOffset;
    // this[materialName].diffuseTexture.vOffset = vOffset;

    this.scene.getMeshByID(meshID).material = this[materialName];

  }

  createText = (meshID, text, font, color) => {

    const materialName = `material-${meshID}`;
    const textureName = `texture-${meshID}`;

    if(this[materialName]){
      this[materialName].dispose();
    }
    if(this[textureName]){
      this[textureName].dispose();
    }

    this[textureName] = new BABYLON.DynamicTexture(textureName, {width:1024, height:1024}, this.scene);   
    this[textureName].hasAlpha = true;
    
    this[materialName] = new BABYLON.StandardMaterial(materialName, this.scene);    				
    this[materialName].diffuseTexture = this[textureName];
    this.scene.getMeshByID(meshID).material = this[materialName];

    this[textureName].drawText(`${text}`, 20, 30, font, color, "transparent", true, true);
	
  }

  render() {

    const {modelsLoaded, parts} = this.state;

    return (
      <div className="page customiser-page">
        <Scene onSceneMount={this.onSceneMount} />

        <div id="nav-controls-customiser" className="container-fluid">
          <div className="row">
            <div className="col-6">
              <button className="btn btn-lg btn-outline-primary mr-2" onClick={ (e) => { history.goBack(); } }><i className="fas fa-arrow-left"></i></button>
            </div>
            <div className="col-6 text-right">
              <button className="btn btn-lg btn-outline-primary mr-2" onClick={ (e) => { this.resetCamera(); } }><i className="fas fa-redo"></i> Reset</button>
              <button className="btn btn-lg btn-primary" onClick={ (e) => {  } }><i className="fas fa-check"></i> Finish</button>
            </div>
          </div>
        </div>

        <div id="controls">
      
          {modelsLoaded &&
            <React.Fragment>
              {parts.map( (part, idx) => {
                switch(part.type){
                  case 'color':
                  return <ColorTool key={idx} id={part.id} changeColor={this.changeColor} name={part.name} mesh={part.mesh} />
                  break;
                  case 'pattern':
                  return <PatternTool key={idx} id={part.id} changePattern={this.changePattern} name={part.name} mesh={part.mesh} />
                  break;
                  case 'text':
                  return <TextTool key={idx} id={part.id} createText={this.createText} name={part.name} mesh={part.mesh} />
                  break;
                  default:
                  break;
                }
              })}
            </React.Fragment>
          }
        
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  public: state.portal.public
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Customiser);