import React from 'react';
import ReactDOM from 'react-dom';
import Customiser from './Customiser';

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

const initialState = {}; 
const mockStore = configureStore();

// WebGL not supported in test

it('renders without crashing', () => {
  // const store = mockStore(initialState)
  // const div = document.createElement('div');
  
  // ReactDOM.render(
  //   <Provider store={store}>
  //     <Customiser />
  //   </Provider>, div);
  // ReactDOM.unmountComponentAtNode(div);
});
