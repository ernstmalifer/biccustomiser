import React from 'react';

const colors = [
  { name: 'White', hex: '#FFFFFF'},
  { name: 'Baby Blue', hex: '#00C9F1'},
  { name: 'Sky Blue', hex: '#0097DD'},
  { name: 'Blue', hex: '#0073C1'},
  { name: 'Dark Blue', hex: '#004182'},
  { name: 'Indigo', hex: '#060B49'},
  { name: 'Dark Green', hex: '#00432F'},
  { name: 'Green', hex: '#008546'},
  { name: 'Yellow', hex: '#FCF900'},
  { name: 'Gold', hex: '#FFDF00'},
  { name: 'Orange', hex: '#FF7600'},
  { name: 'Crimson', hex: '#FF0036'},
  { name: 'Maroon', hex: '#990000'},
  { name: 'Purple', hex: '#560083'},
  { name: 'Pink', hex: '#D979B8'},
];

const ColorTool = props => {

  const { id, name, mesh, changeColor } = props;

  return (
    <div className="accordion mb-2" id={`control-${id}`}>
      <div className="card">
        <div className="card-header" id={`control-${id}-heading`}>
          <h5 className="mb-0">
            <button className="font-weight-bold btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target={`#control-${id}-body`} >
              {name}
            </button>
          </h5>
        </div>

        <div id={`control-${id}-body`} className="collapse" data-parent={`#control-${id}`}>
          <div className="card-body">
            {colors.map( (color, idx) => {
              return (
                <a key={`color-${idx}`} className="btn btn-color mr-1 mb-1" style={{backgroundColor: `${color.hex}`}} onClick={() => changeColor(mesh, color.hex)}></a>
              )
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ColorTool;