import React, { Component } from 'react';

import Modal from './../../Modal/Modal';

import $ from 'jquery'; 

const samplePatterns = [
  { name: 'Transparent', url: 'http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/transparent.png'},
  { name: 'Zebra', url: 'http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/zebra.png'},
  { name: 'Cheetah', url: 'http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/cheetah.jpg'},
  { name: 'Impact', url: 'http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/impact.png'},
  { name: 'Pikachu', url: 'http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/pikachu.jpg'},
];

class PatternTool extends Component {

  state = {
    patterns: []
  }

  componentDidMount = () => {
    this.props.changePattern(this.props.mesh, samplePatterns[0].url);
  }

  loadPatterns = () => {
    this.setState({patterns: samplePatterns})
  }

  showInfoModal = () => {
    $('#patternTool-info-modal').modal('show');
  }

  hideInfoModal = () => {
    $('#patternTool-info-modal').modal('hide');
  }

  componentWillUnmount = () => {

  }

  render() {

    const { id, name, mesh, changePattern } = this.props;
    const { patterns, showInfoModal } = this.state;

    return (
      <React.Fragment>
        <div className="accordion mb-2" id={`control-${id}`}>
          <div className="card">
            <div className="card-header" id={`control-${id}-heading`}>
              <h5 className="mb-0">
                <button className="font-weight-bold btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target={`#control-${id}-body`} >
                  {name}
                </button>
              </h5>
            </div>
    
            <div id={`control-${id}-body`} className="collapse" data-parent={`#control-${id}`}>
              <div className="card-body">
                {patterns.length > 0 &&
                  <React.Fragment>
                    {patterns.map( (pattern, idx) => {
                    return (
                      <a key={`pattern-${idx}`} className="btn btn-pattern mr-1 mb-1" style={{backgroundImage: `url(${pattern.url})`}} onClick={() => changePattern(mesh, pattern.url)}></a>
                      )
                    })}
                    <div className="mt-2">
                      <button className="btn btn-primary mr-2" onClick={ (e) => { this.loadPatterns(); } }><i className="fas fas-redo"></i> Reload</button>
                      <button className="btn btn-outline-primary mr-2" onClick={ (e) => { this.showInfoModal(); } }><i className="fas fa-question"></i> Help</button>
                    </div>
                  </React.Fragment>
                }
                {patterns.length === 0 &&
                  <div className="alert alert-primary mb-0" role="alert">
                    <h6 className="alert-heading">Upload files with your Phone or PC</h6>
                    <p>Click lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, consectetur.</p>
                    <button className="btn btn-outline-primary mr-2" onClick={ (e) => { this.loadPatterns() } }>View Files</button>
                    <button className="btn btn-outline-primary mr-2" onClick={ (e) => { this.showInfoModal(); } }><i className="fas fa-question"></i> Help</button>
                  </div>
                }
              </div>
            </div>
          </div>
        </div>

        <Modal>
          <div id="patternTool-info-modal" className="modal fade" tabIndex="-1" role="dialog">
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-body text-center">
                  <h3 className="mt-5 mb-2">Please go to this link: <br/><a href="#">bic.com/pair</a></h3>
                  <h1 className="text-code text-primary">2107</h1>
                  <p className="mb-5 ml-3 mr-3">Using your phone or your computer, go to the provided link and enter the provided code above to start uploading your artwork.</p>
                  <div className="mb-4 ml-3 mr-3">
                    <button type="button" className="btn btn-lg btn-primary btn-block" data-dismiss="modal">Okay</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>

      </React.Fragment>
    );
  }

};

export default PatternTool;