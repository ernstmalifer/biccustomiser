import React, { Component } from 'react';
import update from 'immutability-helper';

const colors = [
  { name: 'White', color_name: 'White'},
  { name: 'Black', color_name: 'Black'},
  { name: 'Blue', color_name: 'Blue'},
  { name: 'Green', color_name: 'Green'},
  { name: 'Yellow', color_name: 'Yellow'},
  { name: 'Orange', color_name: 'Orange'},
  { name: 'Red', color_name: 'Red'},
  { name: 'Purple', color_name: 'Purple'},
  { name: 'Pink', color_name: 'Pink'},
  { name: 'Brown', color_name: 'Brown'},
];

class TextTool extends Component {

  state = {
    text: '',
    font: {
      bold: false,
      italic: false,
      size: 30,
      style: 'Arial'
    },
    color: 'Black'
  }

  componentDidMount = () => {
    this.createText();
  }

  createText = () => {
    const font = `${this.state.font.bold ? `bold` : ``} ${this.state.font.italic ? `italic` : ``} ${this.state.font.underline ? `underline` : ``} ${this.state.font.size}px ${this.state.font.style}`;
    this.props.createText(this.props.mesh, this.state.text, font, this.state.color);
  }

  render() {

    const { id, name, mesh, createText } = this.props;

    return (
      <div className="accordion mb-2" id={`control-${id}`}>
        <div className="card">
          <div className="card-header" id={`control-${id}-heading`}>
            <h5 className="mb-0">
              <button className="font-weight-bold btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target={`#control-${id}-body`} >
                {name}
              </button>
            </h5>
          </div>
  
          <div id={`control-${id}-body`} className="collapse" data-parent={`#control-${id}`}>
            <div className="card-body">

              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text"><i className="fas fa-font" /></span>
                </div>
                <input type="text" className="form-control" placeholder="Text" onChange={(e) => 
                  {
                    this.setState(update(this.state, {text: {$set: e.target.value}}), () => {
                      this.createText();
                    })
                  }
                } />
              </div>

              <div className="input-group mb-3">
                <div className="btn-group mr-2" role="group">
                  <button type="button" className={`btn ${this.state.font.bold ? `btn-primary` : `btn-light`}`} onClick={() => {
                    this.setState(update(this.state, {font: {bold: {$set: !this.state.font.bold}}}), () => {
                      this.createText();
                    })
                  }}><i className="fas fa-bold"></i></button>
                  <button type="button" className={`btn ${this.state.font.italic ? `btn-primary` : `btn-light`}`} onClick={() => {
                    this.setState(update(this.state, {font: {italic: {$set: !this.state.font.italic}}}), () => {
                      this.createText();
                    })
                  }}><i className="fas fa-italic"></i></button>
                </div>
                <div className="mr-2" role="group">
                  <select style={{width: '150px'}} className="form-control" id="font-control" value={this.state.font.value} onChange={(e) => {
                    this.setState(update(this.state, {font: {style: {$set: e.target.value}}}), () => {
                      this.createText();
                    })
                  }}>
                    {
                      // This might be system fonts
                    }
                    <option value="Arial">Arial</option>
                    <option value="Century Gothic">Century Gothic</option>
                    <option value="Comic Sans MS">Comic Sans MS</option>
                    <option value="Georgia">Georgia</option>
                    <option value="Impact">Impact</option>
                    <option value="Lucida Console">Lucida Console</option>
                    <option value="Monotype Corsiva">Monotype Corsiva</option>
                    <option value="Times New Roman">Times New Roman</option>
                    <option value="Trebuchet MS">Trebuchet MS</option>
                    <option value="Verdana">Verdana</option>
                  </select>
                </div>
                <div className="" role="group">
                  <input style={{width: '100px'}} type="number" className="form-control" id="font-size-control" value={this.state.font.size} onChange={(e) => {
                    this.setState(update(this.state, {font: {size: {$set: parseInt(e.target.value)}}}), () => {
                      this.createText();
                    })
                  }}/>
                </div>
              </div>

              <div className="input-group">
                {colors.map( (color, idx) => {
                  return (
                    <a key={`color-${idx}`} className="btn btn-color mr-1 mb-1" style={{backgroundColor: `${color.color_name}`}} onClick={() => {     this.setState(update(this.state, {color: {$set: color.color_name}}), () => {
                        this.createText();
                      })
                    }}></a>
                  )
                })}
              </div>

            </div>
          </div>
        </div>
      </div>
    );
  }

};

export default TextTool;