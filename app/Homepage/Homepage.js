/* eslint-disable */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import './Homepage.css';

class Homepage extends Component {
  render() {
    return (
      <div id="homepage" className="page">
        <h1>Homepage</h1>
        <p>Tetetest</p>
        <Link to="/products" className="btn btn-primary">
          Catalog
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Homepage);
