import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import './Product.css';

class Product extends Component {

  render() {

    const productID = this.props.match.params.id;

    return (
      <div id="product" className="page">
        <h1>Product Name</h1>
        <Link to={`/customise?productID=${productID}`} className="btn btn-primary">Customise</Link>
      </div>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Product);