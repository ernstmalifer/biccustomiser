import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  loadProducts
} from '../reducers/products';

import './Products.css';

class Products extends Component {

  componentDidMount = () => {
    this.props.loadProducts();
  }

  render() {

    const products = this.props.products;

    return (
      <div id="products" className="page">
        <h1>Products</h1>

        {products &&
          <ul id="products">
            {products.map(product => {
              return (
                <li key={`product-${product.id}`} id={`product-${product.id}`} className="product">
                  <h3>{product.name}</h3>
                  <Link to={`/products/${product.id}`} className="btn btn-primary">Product Description</Link>
                </li>
              )
            })}
          </ul>
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products.products
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadProducts
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Products);