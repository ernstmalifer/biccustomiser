import React from 'react';
import ReactDOM from 'react-dom';
import Products from './Products';

import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

const initialState = {
  products: {
    products: []
  }
}; 
const mockStore = configureStore();

it('renders without crashing', () => {
  const store = mockStore(initialState)
  const div = document.createElement('div');
  
  ReactDOM.render(
    <Provider store={store}>
      <Products />
    </Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
