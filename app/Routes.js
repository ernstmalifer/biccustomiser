import React from 'react';
import { Switch, Route } from 'react-router';
import App from './containers/App';
import { history } from './store/configureStore';

import { CSSTransition, TransitionGroup } from 'react-transition-group';

import AttractLoop from './AttractLoop/AttractLoop';
import Homepage from './Homepage/Homepage';
import Products from './Products/Products';
import Product from './Product/Product';
import Customiser from './Customiser/Customiser';
import Checkout from './Checkout/Checkout';

import { Link } from 'react-router-dom';

export default () => (
  <App>

    <div id="nav-controls" className="container-fluid">
      <div className="row">
        <div className="col-6">
          {location.hash !== '#/' &&
            <React.Fragment>
              <button className="btn btn-lg btn-primary mr-2" onClick={ (e) => { history.goBack(); } }><i className="fas fa-arrow-left"></i></button>
              <Link to="/" className="btn btn-lg btn-primary mr-2"><i className="fas fa-home"></i> Home</Link>
            </React.Fragment>
          }
          {location.hash === '#/' &&
            <Link to="/" className="btn btn-lg btn-primary btn-noevent">Homepage</Link>
          }
        </div>
        <div className="col-6 text-right">
          <Link to="/products" className="btn btn-lg btn-primary mr-2"><i className="fas fa-search"></i> Search</Link>
          <Link to="/checkout" className="btn btn-lg btn-primary mr-2"><i className="fas fa-shopping-cart"></i> View Cart</Link>
        </div>
      </div>
    </div>

    <Route render={({ location }) => (
      <div className="route-container" route={`route-${location.pathname}`}>
        <Switch>
          <Route exact path="/attractloop" component={AttractLoop}/>
          <Route exact path="/" component={Homepage}/>
          <Route exact path="/products" component={Products}/>
          <Route exact path="/products/:id" component={Product}/>
          <Route exact path="/customise" component={Customiser}/>
          <Route exact path="/checkout" component={Checkout}/>
          <Route render={() => <div>Not Found</div>} />
        </Switch>
      </div>
    )} />

    <div id="modal-container"></div>

  </App>
);
