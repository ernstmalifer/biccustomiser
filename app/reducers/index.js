// @flow
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import portal from './portal';
import products from './products';

export default function createRootReducer(history: History) {
  return combineReducers({
    router: connectRouter(history),
    portal,
    products
  });
}
