export const SETDEBUG = 'portal/SETDEBUG';

const initialState = {
  debug: false,
  environment: window.environment,
  endpoint: `${API_HOST}`,
  public: `${PUBLIC}:${PORT}/`,
  key: KEY,
  password: PASSWORD,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SETDEBUG:
      return {
        ...state,
        debug: true
      }

    default:
      return state
  }
};


export const setDebug = () => {
  return (dispatch, getState) => {
    dispatch({
      type: SETDEBUG
    });
  }
};