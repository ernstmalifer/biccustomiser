import axios from 'axios';

export const PRODUCTS_REQUESTED = 'products/PRODUCTS_REQUESTED';
export const PRODUCTS_FINISHED = 'products/PRODUCTS_FINISHED';
export const PRODUCTS_SET = 'products/PRODUCTS_SET';

const initialState = {
  products: [],
  isLoadingProducts: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
  case PRODUCTS_REQUESTED:
    return {
      ...state,
      isLoadingProducts: true
    };

  case PRODUCTS_FINISHED:
    return {
      ...state,
      isLoadingProducts: false
    };

  case PRODUCTS_SET:
    return {
      ...state,
      products: action.products
    };

  default:
    return state;
  }
};

export const loadProducts = (cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: PRODUCTS_REQUESTED
    });

    axios.get(`${getState().portal.public}products.json`).then(response => {

      dispatch({
        type: PRODUCTS_SET,
        products: response.data
      });

      dispatch({
        type: PRODUCTS_FINISHED
      });

      if (cb instanceof Function) { cb(); }
      
    })

  };
};
