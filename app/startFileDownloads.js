const { DownloaderHelper } = require('node-downloader-helper');
const fs = require('fs');
const rimraf = require('rimraf');
const path = require('path');
const downloadsFolder = require('downloads-folder');
const log = require('electron-log');

const bicDownloadsFolder = path.join(downloadsFolder() , 'bic');

export const startFileDownloads = (cb) => {

  if (!fs.existsSync(bicDownloadsFolder)){
    fs.mkdirSync(bicDownloadsFolder);
    log.info('Creating BIC downloads folder');
  }
  
  rimraf(bicDownloadsFolder + '/*', () => {
    log.info('Emptying BIC downloads folder');
  });

  const downloadProducts = new DownloaderHelper(
    'https://biccustomiserportal.engagisdemo.com.au/wp-json/wc/v2/products?consumer_key=ck_f216384f5e9db05dba249d0b487067d6190e3784&consumer_secret=cs_c71b5becdea75797d1beb5b0ce425cd07f9b9ef9',
    bicDownloadsFolder,
    {
        fileName: 'products.json'
    }
  );

  log.info('Starting download products');
  downloadProducts.start();

  downloadProducts.on('end', () => {
    log.info('Finished downloading products');

    // Here parses the products.json
    log.info('Parsing products to be added to the downloader');

    const promises = [];
    const filesToDownload = [
      {
        "url": "http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/Pen.obj",
        "name": "Pen.obj"
      },
      {
        "url": "http://biccustomiserportal.engagisdemo.com.au/wp-content/uploads/Pen.mtl",
        "name": "Pen.mtl"
      },
    ]

    filesToDownload.forEach((file) => {
      // Check here if the file is already downloaded

      promises.push(promiseDownloaderMaker(file.url, file.name))
    })


    Promise.all([...promises]).then((values) => {
      log.info('FINISHED DOWNLOADS');
      if(typeof(cb) === 'function'){
        cb();
      }
    });

  });

}

const promiseDownloaderMaker = (url, name) => {
  return new Promise(function(resolve, reject) {
    const download = new DownloaderHelper(
      url,
      bicDownloadsFolder);

    log.info(`Starting download ${name}`);
    download.start();

    download.on('end', () => {
      log.info(`Finished download ${name}`);
      resolve();
    });
  });
}