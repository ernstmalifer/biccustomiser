import config from './config';

const express = require('express')
const path = require('path');
const app = express();
const downloadsFolder = require('downloads-folder');
const serveIndex = require('serve-index');

const log = require('electron-log');

const bicDownloadsFolder = path.join(downloadsFolder() , 'bic');


const port = config.PORT;
var server = null;


export const startFileDownloadsServer = () => {

  app.use(express.static(bicDownloadsFolder), serveIndex(bicDownloadsFolder, {'icons': true}));

  server = app.listen(port, () => {
    log.info('Starting BIC downloads folder server listening to port: ' + port);
  }).on('error', () => {
    log.error('BIC downloads folder server already started at port: ' + port);
  });

}

export const stopFileDownloadsServer = () => {
  log.info('Stopping BIC downloads folder server listening to port: ' + port);
  server.close();
}